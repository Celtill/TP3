// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/deco", function(req, res, next) {
	res.clearCookie("token");
	res.redirect('/'); 
});

app.post("/login", function(req, res, next) {
	/* id et mdp de l'utilisateur */
	var id = req.body.login;
	var mdp = req.body.mdp;
	/* On vérifie l'éxistence de cet utilisateur dans la base de données */
	db.all("SELECT * FROM users WHERE ident=? AND password=?;", [id, mdp], function(err, data) {
		if(data.length === 0) {
			//res.json({status : false, location : "localhost:1234"});
			console.log("fail");
			res.redirect('/');
			res.end();
		}
		else {
			/* Le token unique pour identifier la connexion (tokenG pour token généré) */
			var shajs = require("crypto");
			var tokenG = shajs.createHmac('sha256', Math.random().toString() + Date.now() + id + mdp).digest('hex');
			/* On vérifie si l'utilisateur possède déjà une session dans la base de donneés */
			db.all("SELECT * FROM sessions WHERE ident=?;", [id], function(err, data){
				/* Si il en possède une : */
				if(data.length > 0) {
					db.all("UPDATE sessions SET token = ? WHERE ident = ?;", [tokenG, id], function(err, data){
						//res.json({status : true, token : tokenG});

					});
				}
				/* Sinon : */
				else {
					db.all("INSERT INTO sessions VALUES(?,?);", [id, tokenG], function(err, data){
						//res.json({status : true, token : tokenG});
					});
				}
			});
			res.cookie("session", tokenG);
			res.redirect('/pageCo.html');
		}
		return 0;
	});
});



// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});